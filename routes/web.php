<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Главная страница
Route::get('/', [
    'as'   => 'index',
    'uses' => '\App\Http\Controllers\PageController@index',
]);

Route::pattern('slug', '^[A-z0-9_\/]+');

// Форма добавления корневой страницы
Route::get('/add', [
    'uses' => '\App\Http\Controllers\PageController@getAdd',
]);

// Форма добавления корневой страницы
Route::post('/add', [
    'uses' => '\App\Http\Controllers\PageController@postAdd',
]);

// Форма добавления подстраницы
Route::get('{slug}/add', [
    'uses' => '\App\Http\Controllers\PageController@getAdd',
]);

// Добавление подстраницы
Route::post('{slug}/add', [
    'uses' => '\App\Http\Controllers\PageController@postAdd',
]);

// Форма редактирования подстраницы
Route::get('{slug}/edit', [
    'uses' => '\App\Http\Controllers\PageController@getEdit',
]);

// Редактирование подстраницы
Route::post('{slug}/edit', [
    'uses' => '\App\Http\Controllers\PageController@postEdit',
]);

// Просмотр страницы
Route::get('{slug}', [
    'uses' => '\App\Http\Controllers\PageController@show',
]);