<?php

namespace App\Http\Controllers;

use App\Markdown;
use App\Pages;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Главная страница
     */
	public function index()
	{
	    $pages = Pages::getLastPages(5);

	    if (!$pages->isEmpty()) {
	        foreach ($pages as $page) {
                $page->text = Markdown::render($page->text);
            }
        }

		return view('main')->with('pages', $pages);
	}

    /**
     * Просмотр страницы
     *
     * @param string $slug
     * @return $this
     */
    public function show($slug)
    {
        $page = Pages::getPageByUrl($slug);

        if (empty($page)) {
            abort(404);
        }

        $page->text = Markdown::render($page->text);

        return view('page')->with('page', $page);
    }

    /**
     * Форма добавления подстраницы
     *
     * @param string $slug
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdd($slug = '')
    {
        $page = Pages::getPageByUrl($slug . '/add');

        if (empty($page)) {
            return view('form');
        } else {
            return view('page')
                ->with('page', $page);
        }
    }

    /**
     * Добавление подстраницы
     *
     * @param Request $request
     * @param string $slug
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postAdd(Request $request, $slug = '')
    {
        $data            = $request->all();
        $data['fullUrl'] = $slug . '/' . $data['url'];
        $data['fullUrl'] = Pages::preparePageUrl($data['fullUrl']);

        $validator = Validator::make($data, [
            'url'     => 'required|regex:/^[A-z0-9_]+$/',
            'fullUrl' => 'required|unique:pages,url|regex:/^\/[A-z0-9_\/]+\/$/|max:255',
            'title'   => 'required',
            'text'    => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }



        $page = Pages::create([
            'url'   => $data['fullUrl'],
            'title' => $data['title'],
            'text'  => $data['text'],
        ]);

        return redirect($page->url);

    }

    /**
     * Форма редактирования страницы
     *
     * @param $slug
     * @return $this
     */
    public function getEdit($slug)
    {
        $page = Pages::getPageByUrl($slug . '/edit');

        if (!empty($page)) {
            return view('page')
                ->with('page', $page);
        }

        $page = Pages::getPageByUrl($slug);

        if (empty($page)) {
            abort(404);
        }

        preg_match('~^.*\/(.+?)\/$~', $page->url, $matches);
        $page->url = $matches[1];

        if (empty($page)) {
            abort(404);
        }

        return view('form')->with('page', $page);
    }

    /**
     * Сохранение страницы
     *
     * @param Request $request
     * @param $slug
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEdit(Request $request, $slug)
    {
        $page = Pages::findOrFail($request->id);
        $data = $request->all();

        $validator = Validator::make($data, [
            'title'   => 'required',
            'text'    => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $page->title = $data['title'];
        $page->text  = $data['text'];

        $page->save();

        return redirect($page->url);
    }
}
