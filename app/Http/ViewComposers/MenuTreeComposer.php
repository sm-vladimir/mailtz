<?php

namespace App\Http\ViewComposers;

use App\Pages;
use Illuminate\View\View;

class MenuTreeComposer
{
    /**
     * @var array
     */
    protected $menu;

    /**
     * MenuTreeComposer constructor.
     */
    public function __construct()
    {
        $this->menu = Pages::getMenu();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('menu', $this->menu);
    }
}