<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['url', 'title', 'text'];

    /**
     * Возвращает дерево страниц
     *
     * @return array
     */
    public static function getMenu()
    {
        $pages = self::all()->toArray();

        if (empty($pages)) {
            return [];
        }

        $menuTree = [];

        foreach ($pages as &$page) {
            $page['parentUrl'] = preg_replace('~[^\/]+\/$~', '', $page['url']);
            $sortUrl[] = $page['url'];
            $menuTree[$page['url']] = $page;
        }

        array_multisort(
            $sortUrl, SORT_STRING, SORT_ASC,
            $menuTree
        );

        $menuByUrl   = [];
        $menuParents = [];

        foreach ($menuTree as $page) {
            $menuByUrl[$page['url']] = $page;
            $menuParents[$page['parentUrl']][$page['url']] =& $menuByUrl[$page['url']];
        }

        ksort($menuByUrl);
        array_shift($menuParents);
        krsort($menuParents);

        foreach ($menuParents as $parentUrl => $subPages) {
            ksort($subPages);
            foreach ($subPages as $url => $page) {
                if (isset($menuByUrl[$parentUrl])) {
                    $menuByUrl[$parentUrl]['subPages'][$url] =& $menuByUrl[$url];
                }
                unset($menuByUrl[$url]);
            }
        }

        return $menuByUrl;
    }

    /**
     * Получение страницы по ID
     *
     * @param int $id
     * @return mixed
     */
    public static function getPageById($id)
    {
        if (empty($id)) {
            return false;
        }

        return self::find($id);
    }

    /**
     * Получение страницы по url
     * @param string $url
     * @return mixed
     */
    public static function getPageByUrl($url)
    {
        $url = self::preparePageUrl($url);

        return self::where('url', $url)->first();
    }

    /**
     * Возвращает последние созданные страницы
     *
     * @param int $limit
     * @return mixed
     */
    public static function getLastPages($limit = 10)
    {
        return self::orderBy('id', 'DESC')->take($limit)->get();
    }

    /**
     * Преобработка url
     *
     * @param string $url
     * @return string
     */
    public static function preparePageUrl($url)
    {
        $url = '/' . trim($url, '/') . '/';
        $url = preg_replace('~[\/]{2,}~', '/', $url);

        return $url;
    }
}
