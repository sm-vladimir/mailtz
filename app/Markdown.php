<?php

namespace App;

class Markdown
{
    /**
     * Доступные шаблоны
     *
     * @var array
     */
    public static $patterns = array (
        '/(\*\*)(.*?)\1/m' => '<b>$2</b>',                      // Bold
        '/(\\\\\\\\)(.*?)\1/m' => '<i>$2</i>',                  // Italic
        '/\(\(([0-9A-z_\/]+)[\s]+(.+?)\)\)/m' => 'self::link',  // Link
    );

    /**
     * Callback обработки ссылок
     *
     * @param array $regs
     * @return string
     */
    private static function link($regs) {
        $url   = Pages::preparePageUrl($regs[1]);
        $url   = url('/') . $url;
        $title = trim($regs[2]);

        return sprintf('<a href="%s">%s</a>', $url, $title);
    }

    /**
     * Преобразование Markdown в HTML
     *
     * @param string $text
     * @return string
     */
    public static function render($text) {
        $text = "\n" . $text . "\n";

        foreach (self::$patterns as $regex => $replacement) {
            if (is_callable($replacement)) {
                $text = preg_replace_callback($regex, $replacement, $text);
            } else {
                $text = preg_replace($regex, $replacement, $text);
            }
        }

        return trim($text);
    }
}
