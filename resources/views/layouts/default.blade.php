<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Mail.ru | TZ | Vladimir Smolentsev</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/custom.css">
</head>
<body>
    <div class="blog-masthead">
        <div class="container">
            <nav class="nav blog-nav">
                <a class="nav-link{{ url()->route('index') == url()->current() ? ' active' : null }}" href="{{ url()->route('index') }}">Home</a>
            </nav>
        </div>
    </div>

    @yield('header')

    <div class="container">
        <div class="row">
            <div class="col-sm-8 blog-main">
                @yield('content')
            </div><!-- /.blog-main -->
            <div class="col-sm-4 blog-sidebar">
                <div class="sidebar-module">
                    @yield('sidebar')
                </div>
            </div><!-- /.blog-sidebar -->
        </div><!-- /.row -->
    </div><!-- /.container -->
    <footer class="blog-footer">
        <p>Wiki example for <a href="https://mail.ru">@Mail.ru</a> by <a href="mailto:smolen.vladimir@gmail.com">Vladimir Smolentsev</a>.</p>
        <p><a href="#">Back to top</a></p>
    </footer>
</body>
</html>