@extends('layouts.default')

@section('content')
    @include('pages.page')
@endsection

@section('sidebar')
    @include('sidebar.menu')
@stop