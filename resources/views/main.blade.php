@extends('layouts.default')

@section('header')
    <div class="blog-header">
        <div class="container">
            <h1 class="blog-title">The Bootstrap Wiki</h1>
            <p class="lead blog-description">An example Wiki built with Bootstrap.</p>
        </div>
    </div>
@endsection

@section('content')
    @include('pages.list')
@endsection

@section('sidebar')
    <div class="form-group">
        <a class="btn btn-primary btn-sm" href="{{ url('/') }}/add">Добавить корневую страницу</a>
    </div>
    @include('sidebar.menu')
@stop