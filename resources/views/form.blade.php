@extends('layouts.default')

@section('content')
    @if (empty($page))
        @include('pages.add')
    @else
        @include('pages.edit')
    @endif
@endsection

@section('sidebar')
    @include('sidebar.menu')
@stop