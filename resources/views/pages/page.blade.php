@if (!empty($page))
    <div class="blog-post">
        <h3>{{$page->title}}</h3>
        <p class="blog-post-meta">{{ (new DateTime($page->created_at))->format('D, d M Y')}}</p>
        {!! $page->text !!}
    </div><!-- /.blog-post -->
    <div class="form-group">
        <a class="btn btn-primary btn-sm" href="{{ url()->current() }}/add">Добавить подстраницу</a>
        <a class="btn btn-primary btn-sm" href="{{ url()->current() }}/edit">Редактировать страницу</a>
    </div>
@endif