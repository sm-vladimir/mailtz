{!! Form::open(['url' => url()->current(), 'action' => 'Controller@postAdd']) !!}

<div class="form-group">
    <label for="url">Url</label>
    <input type="text" class="form-control" id="url" name="url" value="{{ old('url') }}">
    @if ($errors->has('url'))
        <div class="invalid-feedback">{{ $errors->first('url') }}</div>
    @elseif($errors->has('fullUrl'))
        <div class="invalid-feedback">{{ $errors->first('fullUrl') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}">
    @if ($errors->has('title'))
        <div class="invalid-feedback">{{ $errors->first('title') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="text">Text</label>
    <textarea class="form-control" id="text" rows="10" name="text">{{ old('text') }}</textarea>
    @if ($errors->has('text'))
        <div class="invalid-feedback">{{ $errors->first('text') }}</div>
    @endif
</div>

<a href="{{ url()->previous() }}" class="btn btn-primary">Go back</a>
<button type="submit" class="btn btn-primary">Add page</button>
<br/>
<br/>

{!! Form::close() !!}