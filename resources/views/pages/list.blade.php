@if (!empty($pages))
    @foreach($pages as $page)
        <div class="blog-post">
            <h3>{{$page->title}}</h3>
            <p class="blog-post-meta">{{ (new DateTime($page->created_at))->format('D, d M Y')}}</p>

            {!! \Illuminate\Support\Str::limit($page->text, 200) !!}
            <br/>
            <a href='{{$page->url}}'>Read more...</a>
        </div><!-- /.blog-post -->
    @endforeach
@endif