@foreach($submenu as $page)
    <li>
        @if (!empty($page['subPages']))
            <a href="{{ $page['url'] }}">{{ $page['title'] }}</a>
            <ul>
                @include('sidebar.submenu', ['submenu' => $page['subPages']])
            </ul>
        @else
            <a href="{{ $page['url'] }}">{{ $page['title'] }}</a>
        @endif
    </li>
@endforeach